var should = require('should');
var request = require('request');
var expect = require('chai').expect;
var baseUrl = 'http://localhost:3000';
var book_id;
var chai = require('chai');

describe('get books',function(){
    it('returns luke',function(done){
        console.log(baseUrl+'/books');
        request.get({url:baseUrl+'/books'},function(error,response,body){
            let bodyObj = JSON.parse(body);
            book_id = bodyObj.books[0].id;
            expect(bodyObj.books).to.be.an('array');
            expect(response.statusCode).to.equal(200);
            console.log(body);
            done(); 
        })
    })
})


describe('get books detail',function(){
    it('returns luke',function(done){
        console.log(baseUrl+'/books/'+book_id);
        request.get({url:baseUrl+'/books/'+book_id},function(error,response,body){
            let bodyObj = JSON.parse(body);
            expect(bodyObj.id).to.be.a('number');
            expect(bodyObj.numberOfPage).to.be.a('number');
            expect(bodyObj.publishedOn).to.be.a('number');
            expect(bodyObj.author).to.be.a('string');
            expect(bodyObj.isbn).to.be.a('string');
            expect(bodyObj.title).to.be.a('string');
            expect(response.statusCode).to.equal(200);
            console.log(body);
            done(); 
        })
    })
})

describe('create new books',function(){
    it('create new books',function(done){
        request.post({
            url:baseUrl+'/books',
            json:{
                title: "string2",
                author: "string2",
                isbn: "string2",
                publishedOn: 2019,
                numberOfPage: 188
              }
        },function(error,response,body){
            expect(response.statusCode).to.equal(200);
            console.log(body);
            console.log(error);
            done();
        })
    })
})

describe('update books',function(){
    it('update books',function(done){
        request.patch({
            url:baseUrl+'/books/'+book_id,
            json:{
                title: "string2",
                author: "string2",
                isbn: "string2",
                publishedOn: 2019,
                numberOfPage: 188
              }
        },function(error,response,body){
            expect(response.statusCode).to.equal(200);
            console.log(body);
            console.log(error);
            done();
        })
    })
})

describe('delete books',function(){
    it('delete books',function(done){
        request.delete({
            url:baseUrl+'/books/'+book_id,
        },function(error,response,body){
            expect(response.statusCode).to.equal(200);
            console.log(body);
            console.log(error);
            done();
        })
    })
})

