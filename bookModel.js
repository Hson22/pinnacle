var mongoose = require('mongoose');

var bookSchema = mongoose.Schema({
    id:{
        type : Number,
        require:true
    },
    title:{
        type : String,
        require:true
    },
    author:{
        type: String,
        require:true
    },
    isbn:{
        type: String,
        require:true
    },
    publishedOn:{
        type: Number,
        require:true
    },
    numberOfPage:{
        type:Number,
        require:true
    }
});

var BookModel = module.exports = mongoose.model('books',bookSchema);

module.exports.createBook = function(book,callback){
    let newBook = {
        id: Math.floor(Math.random()*90000) + 10000,
        title: book.title,
        author: book.author,
        isbn: book.isbn,
        publishedOn: book.publishedOn,
        numberOfPage: book.numberOfPage,
    }

    BookModel.create(newBook,callback);
}

module.exports.getBooks = function(callback,limit){
    BookModel.find(callback).limit(limit);
};

module.exports.getBooksById = function(id,callback){
    BookModel.find({id:id},callback);
}
module.exports.deleteBoksById = function(id,callback){
    BookModel.findOneAndDelete({id:id},callback);
}

module.exports.updateBook = function(id,book,callback){
    let query = {id:id};
    let newBook = {
        title: book.title,
        author: book.author,
        isbn: book.isbn,
        publishedOn: book.publishedOn,
        numberOfPage: book.numberOfPage,
    }
    BookModel.findOneAndUpdate(query,newBook,callback);
}