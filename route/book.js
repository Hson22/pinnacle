var express = require('express');
var router = express.Router();

var bookModel = require('./../bookModel');

router.post('/books',function(req,res){
    let newBook = req.body;
    bookModel.createBook(newBook,function(err,book){
        if(err) {
            throw err;
        }
        res.json(book);
    })
});

router.get('/books',function(req,res){
    bookModel.getBooks(function(err,books){
        if(err) {
            throw err;
        }
        let book = {
            // status:'success',
            // message:'success get list',
            books:books
        }
        res.json(book);
    });
});

router.get('/books/:id',function(req,res){
    bookModel.getBooksById(req.params.id,function(err,book){
        res.json(book[0]);
    })
})

router.delete('/books/:id',function(req,res){
    bookModel.deleteBoksById(req.params.id,function(err,book){
        res.send('');
    })
})

router.patch('/books/:id',function(req,res){
    bookData = req.body;
    bookModel.updateBook(req.params.id,bookData,function(err,callback){
        if(err){
            throw err;
        }
        res.send('');
    })
})

module.exports = router;